# Application du Design Pattern GoF "Composite"
class Sonde:
    pass

class SondeSimple(Sonde):    
    def __init__(self, v):
        self.v = v
    def afficher(self):
        print(self.v)
    def v_max(self): return self.v
    def nb_sondes(self): return 1
    def v_moyenne(self): return self.v
    
class GroupeDeSondes(Sonde):
    def __init__(self, sondes): # sondes est une liste de Sonde
        self.sondes = sondes
    def afficher(self):
        for s in self.sondes:
            s.afficher()
    def v_max(self): 
        max=-1e10
        for s in self.sondes :
            if max<s.v_max():
                max=s.v_max()
        return max
    def nb_sondes(self): 
        nb=1
        for s in self.sondes :
            nb+=s.nb_sondes()
        return nb    
    def v_moyenne(self): 
        nb=1
        somme=0
        for s in self.sondes :
            nb+=s.SondeSimple()
            
        for s in self.sondes :
            somme+=s.v_moyenne()
  
        
        return somme/nb        




        
s1 = SondeSimple(18)
s2 = SondeSimple(11)
gs1 = GroupeDeSondes([s1, s2])
s3 = SondeSimple(8)
gs2 = GroupeDeSondes([s3, gs1])
gs2.afficher()

print(s1.v_max())
print(gs2.v_max())

print(gs2.nb_sondes())

print(gs2.v_moyenne())
