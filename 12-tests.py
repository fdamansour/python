# Tests
import re

class correction :
    def mettre_en_minuscules(self, s):
        """ Met en minuscules uniquement la chaine
        
        >>> c = correction()
        >>> c.mettre_en_minuscules("Hello !")
        'hello !'
        """
        return s.lower()
    
    def enlever_telephones(self, s):
        """ ---
        
        >>> d = correction()
        >>> d.enlever_telephones("This is my phone number 0987675486 !")
        'This is my phone number ********** !'
        """        
        return re.sub("[0-9]+", "**********", s)

if __name__=="__main__":
    import doctest
    doctest.testmod()
    print("Tests finis !")