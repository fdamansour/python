# un operateur lit un code d'erreur sur un appareil et l'analyse ici
try:
    n = int(input("Code ?"))
    print("Code d'erreur : ", n)
    
    # exemple :
    # 1 => erreurs 1
    # 2 => erreurs 2
    # 4 => erreurs 3
    # 8 => erreurs 4
    # 5 => erreurs 1 et 3
    # 10 => erreurs 2 et 4
    # 7 => erreurs 1 et 2 et 3
    num = 0
    bit = 1
    s = ""
    sep = ""
    while num < n:
        num = 2**(bit-1)
        if num & n != 0:
            s += sep+str(bit)
            sep = " et "
        bit += 1
    print(s)
    
except ValueError:
    print("ValueError !")
