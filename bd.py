
# Acces à une base SQLite 3
import sqlite3
import datetime
conn = sqlite3.connect("C:/Users/DELL 5470/Desktop/python/log.sqlite")
conn.execute("CREATE TABLE IF NOT EXISTS messages(id INTEGER PRIMARY KEY, info TEXT)")

# écriture
INFO = "Il est : "+str(datetime.datetime.now())
conn.execute("INSERT INTO messages(info) VALUES(?)", (INFO,) )
conn.commit()

# lecture
c = conn.cursor()
c.execute("SELECT * FROM messages")
for ligne in c.fetchall():
    print(ligne)
c.close()

conn.close()
